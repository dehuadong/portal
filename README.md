ThinkCMF企业门户解决方案6.0
===============
帮助你更快地搭建企业门户网站

### 开发手册

https://www.thinkcmf.com/docs/cmf6/#/

https://www.kancloud.cn/thinkcmf/theme_tutorial

### API接口文档

### Git仓库

1. 码云:https://gitee.com/thinkcmf/portal 主要仓库
2. GitHub:https://github.com/thinkcmf/portal 同步仓库

### 环境推荐
> php7.4

> mysql 5.7+

> 打开rewrite


### 最低环境要求
> php7.4+

> mysql 5.7+

> 打开rewrite

### 安装步骤
#### 方法一 （已安装thinkcmf）
1. 在应用市场安装
2. 然后在后台模板管理里安装`simpleboot3`模板
3. 如果想用`simpleboot3`模板，可以启用此模板
#### 方法二 （未安装thinkcmf）
1. 下载thinkcmf仓库代码，安装
2. 在应用市场安装
3. 然后在后台模板管理里安装`simpleboot3`模板
4. 如果想用`simpleboot3`模板，可以启用此模板

详细安装步骤请查看[ThinkCMF FAQ](https://www.kancloud.cn/thinkcmf/faq/1005840)



### QQ群:
`ThinkCMF 官方交流群`:316669417  
   
`ThinkCMF 高级交流群`:100828313 (付费)  

`ThinkCMF 铲屎官交流群`:415136742 (生活娱乐，为有喵的猿人准备)


### 反馈问题
https://gitee.com/thinkcmf/portal/issues

### 更新日志
#### 1.0.0
* 移除所有演示应用、插件、模板代码





